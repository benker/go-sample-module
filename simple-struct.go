package module

import (
	"fmt"
)

type goSampleModule struct {
	version string
	developer string
}

// NewGoSampleModule create a goSampleModule
func NewGoSampleModule() *goSampleModule {
	return &goSampleModule {
		version: "v2.0.0",
		developer: "benker",
	}
}

func (m *goSampleModule) String() string {
	return fmt.Sprintf("version %s, developer %s", m.version, m.developer)
}
