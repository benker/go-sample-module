package main

import (
	"log"
	module "gitlab.com/benker/go-sample-module/v2"
)

func main() {
	m := module.NewGoSampleModule()
	log.Printf("%s", m.String())
}
